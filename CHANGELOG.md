<!-- @formatter:off -->
# Changelog

All notable changes to this project will be documented in this file.

## [%1.0.1] - 2022-11-27

_See what changed: [1.0.0...1.0.1](https://gitlab.com/michewl/midnight-theme/highlightjs/-/compare/1.0.0...1.0.1)_

### 🐛 Bug Fixes

- [[`dff30461`](https://gitlab.com/michewl/midnight-theme/highlightjs/-/commit/dff304611ff62b3a9fc257a1c936823cdba231c6)] Missing comma and `doctag` color value

## [%1.0.0] - 2022-11-24

_See what changed: [0.1.0...1.0.0](https://gitlab.com/michewl/midnight-theme/highlightjs/-/compare/0.1.0...1.0.0)_

### 📝 Documentation

- [[`4498d26f`](https://gitlab.com/michewl/midnight-theme/highlightjs/-/commit/4498d26f16adff772046df2ba7cc5a8bee18b1b3)] _(readme):_ Add missing footnote

### 🔨 Miscellaneous Tasks

- [[`ade71176`](https://gitlab.com/michewl/midnight-theme/highlightjs/-/commit/ade711766d4e1a959b985e02c4c860de02f6cdb0)] Change `built_in` and `attribute` to match intellij

## [%0.1.0] - 2022-11-23

### 🔨 Miscellaneous Tasks

- [[`a1f831e4`](https://gitlab.com/michewl/midnight-theme/highlightjs/-/commit/a1f831e412e29345f0e555c2972aa8fb921ebeac)] Initial commit

<!-- @formatter:on -->
