# Welcome to the contributing guide

Thanks for considering contributing to the midnight theme for the highlight.js library.

Following these guidelines ensures a healthy climate, and the productivity in this project.

We expect that all members of our community follow our [Code of Conduct](CODE_OF_CONDUCT.md).
Please make sure you are welcoming and friendly in all of our spaces.

## Using the issue tracker

First things first: **Do NOT report security vulnerabilities in public issues!**
Please disclose responsibly by letting [the team][team-mailto] know upfront.
We will assess the issue as soon as possible on a best-effort basis and will give you an estimate for when we have a fix
and a release available for an eventual public disclosure.

### Search similar issues

Before you create an issue, please use the [issue search] with keywords related to your concern.
Take a look at open **and** closed issues.

If no issue resolves your concern, please feel free to create a [bug report](CONTRIBUTING.md#bug-reports)
or [feature/enhancement request](CONTRIBUTING.md#feature-requests).

### Bug reports

A bug is a _demonstrable problem_ that is caused by the code in the repository.
Good bug reports should contain all the information to reproduce the issue and are extremely helpful.

Guidelines for bug reports:

1. [As mentioned before](CONTRIBUTING.md#search-similar-issues), use the issue search to check if the issue has already
   been reported.
2. Check if the issue has already been fixed in the current development state.
   Try to reproduce it using the latest changes in `main`.
3. Isolate the problem and ideally create a reduced test case.

A good bug report shouldn't leave others needing to chase you up for more information.
Please try to be as detailed as possible in your report.
What is your environment?
What steps will reproduce the issue?
What OS experiences the problem?
Which browser and what version?
What would you expect to be the outcome?
All these details will help people to fix any potential bugs.

**Example:**

> Short and descriptive title for the bug report
>
> A summary of the issue, including detailed information about the system
> the bug has been encountered in. If suitable, include steps to reproduce
> the bug.
>
>     First, do this
>     Then, do this second part
>     And then, when you do that, the bug occurs
>
> Here is a reduced test case to reproduce the bug: \<url>
>
> Some additional information that is related to the issue.
> Maybe some logging output or lines of code you have identified as the
> cause and/or a possible solution.

## Feature requests

Feature requests are welcome, but take a moment to find out whether your idea fits with the scope and aims of the
project.
It's up to you to make a strong case to convince the project's developers of the merits of this feature.
Please provide as much detail and context as possible.

## Merge Requests

We use merge requests to introduce changes to the project.
The branch `main` contains the latest changes.
This branch **should** always be working but **may** be broken at times.

While this project does not exactly follow a strict workflow, [GitLab Flow] is a good introduction and will show the
general idea of the "workflow" that is used in this project.

A merge request is always related to an issue in the [issue tracker].
Merge requests without an issue will not be accepted.

Good merge requests are a fantastic help.
Keep it focused on the scope of the change and avoid containing unrelated commits.
Squash your commits before submitting the merge request.

**Please ask first** before embarking on any significant merge request (e.g. implementing features, refactoring code)
otherwise you risk spending a lot of time working on something that the project's developers might not want to merge
into the project.

### Commits and commit messages

To create a consistent commit history and to be able to generate a changelog from Git history, this project follows
the [conventional commit specification].

In addition to the consistent format, we try to keep the Git history as clean as possible.
Commits of a change will be squashed.
We encourage everyone to do this themselves before creating a merge request.

#### Commit types and scopes

The scopes are **not** exhaustive and can be used as needed.
The only exception is the type and scope combination `chore(release):` with subject prefix
`prepare for version` which **must** be used for releases since the changelog generation will skip these commit
messages.

| Type     | Scope   | Description                                                                                            |
|----------|---------|--------------------------------------------------------------------------------------------------------|
| build    |         | Changes that affect the build system or external dependencies                                          |
| ci       |         | Changes to the CI configuration files and scripts                                                      |
| docs     |         | Documentation only changes                                                                             |
| docs     | readme  | Used for changes of any <code>README.md</code> file                                                    |
| docs     | cl      | Used for changes of any <code>CHANGELOG.md</code> file                                                 |
| docs     | wiki    | Used for changes of any file part of the wiki                                                          |
| feat     |         | A new feature                                                                                          |
| fix      |         | A bug fix                                                                                              |
| perf     |         | A code change that improves performance                                                                |
| refactor |         | A code change that neither fixes a bug nor adds a feature                                              |
| chore    |         | Other changes that don't modify <code>src</code> or <code>test</code> files                            |
| chore    | release | Used for releases. Git-Cliff ignores this when subject starts with <em>prepare for</em>                |
| style    |         | Changes that do not affect the meaning of the code (white-space, formatting, missing semicolons, etc.) |
| test     |         | Adding missing tests or correcting existing tests                                                      |
| revert   |         | Reverts a previous commit                                                                              |

For message footer we primarily use these:

| Prefix          | Description                                                                                 |
|-----------------|---------------------------------------------------------------------------------------------|
| BREAKING CHANGE | The commit introduces breaking changes                                                      |
| Closes          | The commit closes issues or merge requests. List issues separated by comma                  |
| Implements      | The commit implements features. List issues separated by comma                              |
| Co-authored-by  | The commit is co-authored by another person. One line per author, add multiple if necessary |
| Refs            | The commit references other commits by their hash ID. List hash IDs separated by comma      |

### For new Contributors

If you are totally new to contributing to open source software, [here][howto-contribute-github] is a great tutorial get
a good introduction.
It is about GitHub but also applies to GitLab.

1. [Create a public fork,][gitlab-fork] clone your fork, and configure the remotes:
    ```shell
    # Clone your fork of the repo into the current directory
    git clone https://gitlab.com/<your-username>/highlightjs.git midnight-theme-highlightjs
    # Navigate to the newly cloned directory
    cd midnight-theme-highlightjs
    # Assign the original repo to a remote called "upstream" to be able to update your fork
    git remote add upstream https://gitlab.com/midnight-theme/highlightjs.git
    ```
2. Create a new topic branch from the default branch:
    ```shell
    git switch -c <topic-branch-name>
    ```
3. Make your changes and add tests.
4. Update the documentation for your changes.
5. Push the changes to your fork:
    ```shell
    git push [-u] origin <topic-branch-name>
    ```
6. [Open a merge request][gitlab-mr] with a descriptive title and description.

Before opening the merge request, please make sure the topic branch is up-to-date with the latest changes from the
upstream project.
It is also very appreciated if the topic branch gets updated until it is accepted or rejected.

```shell
git pull --rebase upstream main
git push --force-with-lease origin <topic-branch-name>
```

## Attribution

Thanks to [@nayafia][nayafia] for compiling the [CONTRIBUTING-template.md] and all projects mentioned in this template.

<!-- @formatter:off -->
[team-mailto]: mailto:michewl@pm.me?subject=%5BINCIDENT%5D%20Midnight%20Theme%20Highlight.js%20-%20
[issue search]: https://gitlab.com/midnight-theme/highlightjs/-/issues?sort=created_date&state=all
[GitLab Flow]: https://docs.gitlab.com/ee/topics/gitlab_flow.html
[issue tracker]: https://gitlab.com/midnight-theme/highlightjs/-/issues
[conventional commit specification]: https://www.conventionalcommits.org/en/v1.0.0/
[howto-contribute-github]: https://egghead.io/courses/how-to-contribute-to-an-open-source-project-on-github
[gitlab-fork]: https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html#project-forking-workflow
[gitlab-mr]: https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html#when-you-work-in-a-fork
[nayafia]: https://github.com/nayafia
[CONTRIBUTING-template.md]: https://github.com/nayafia/contributing-template/blob/master/CONTRIBUTING-template.md
<!-- @formatter:on -->
