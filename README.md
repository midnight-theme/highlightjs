# Midnight Theme — highlight.js library

<div align="center">
    <img src="etc/midnight-theme-highlightjs-logo.svg" alt="midnight theme highlightjs logo" width="300">
</div>

___

This is the midnight theme repository for the [highlight.js library][project-website].

## Colors

The general definition of the colors and specification how to use them can be found in
the [meta project of the midnight theme][meta].

### Color Scheme

The color scheme uses additional shades of the [defined color palette](README.md#colors).

|     Color      | Description                                                     |
|:--------------:|-----------------------------------------------------------------|
| `#302d32`[^ij] | The background color for deleted changes in a diff.             |
| `#263223`[^ij] | The background color for added changes in a diff.               |

## Release

A public release will be created when a git tag gets pushed to GitLab. The changelog must be added manually to that
release.

The release workflow is as follows:

_Note: `X.X.X` must be replaced with the release version._

1. Update the `CHANGELOG.md` using [git-cliff][git-cliff]
    * Command: `git cliff --config .config/cliff.toml --output CHANGELOG.md --tag X.X.X`
2. Add and commit all changes
    * Command: `git add . && git commit -m "chore(release): prepare for version X.X.X"`
3. Create a signed tag
    * Command: `git tag -s X.X.X -m "chore(release): version X.X.X"`
4. Push everything
    * Command: `git push && git push --tags`

<!-- @formatter:off -->
[^ij]: <small>Taken from the [IntelliJ project][midnight-intellij].</small>

[project-website]: https://highlightjs.org
[meta]: https://gitlab.com/midnight-theme/meta/
[midnight-intellij]: https://gitlab.com/midnight-theme/intellij
[git-cliff]: https://github.com/orhun/git-cliff
<!-- @formatter:on -->

